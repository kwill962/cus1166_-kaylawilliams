import json #parses twitter data
import pandas as pd #manipulates twitter data
import matplotlib.pyplot as plt #creates graphs
import re  #checks if keyword is in a text using regular expressions

#Function to call and return relevant tweets
def word_in_text(word, text):
    word = word.lower()
    text = text.lower()
    match = re.search(word, text)
    if match:
        return True
    return False

#Function retrieving link that start with "http://" or "https://" from a text
def extract_link(text):
    regex = r'https?://[^\s<>"]+|www\.[^\s<>"]+'
    match = re.search(regex, text)
    if match:
        return match.group()
    return ''


def main():


    #Reading Tweets
    print 'Reading Tweets\n'
    tweets_data_path = '../TwitterDemo/twitter_data.txt'

    #Array that reads tweets and translates json
    tweets_data = []
    tweets_file = open(tweets_data_path, "r")
    for line in tweets_file:
        try:
            tweet = json.loads(line)
            tweets_data.append(tweet)
        except:
            continue


    #Structuring Tweets
    print 'Fetching Tweets\n'
    tweets = pd.DataFrame()
    #3 columns to the tweets DataFrame: text column contains the tweet, lang column contains the language of tweet, and country from which the tweet was sent.
    tweets['text'] = map(lambda tweet: tweet['text'], tweets_data)
    tweets['lang'] = map(lambda tweet: tweet['lang'], tweets_data)
    tweets['country'] = map(lambda tweet: tweet['place']['country'] if tweet['place'] != None else None, tweets_data)


    #Analyzing Tweets by Language
    print 'Comparing tweets by language\n'
    tweets_by_lang = tweets['lang'].value_counts()
    #Creates chart using pyplot
    fig, ax = plt.subplots()
    ax.tick_params(axis='x', labelsize=15)
    ax.tick_params(axis='y', labelsize=10)
    ax.set_xlabel('Languages', fontsize=15)
    ax.set_ylabel('Number of tweets' , fontsize=15)
    ax.set_title('Top 5 languages', fontsize=15, fontweight='bold')
    tweets_by_lang[:5].plot(ax=ax, kind='bar', color='pink')
    plt.savefig('tweets_lang', format='png')


    #Analyzing Tweets by Country
    print 'Comparing tweets by country\n'
    tweets_by_country = tweets['country'].value_counts()
    fig, ax = plt.subplots()
    ax.tick_params(axis='x', labelsize=15)
    ax.tick_params(axis='y', labelsize=10)
    ax.set_xlabel('Countries', fontsize=15)
    ax.set_ylabel('Number of tweets' , fontsize=15)
    ax.set_title('Top 5 countries', fontsize=15, fontweight='bold')
    tweets_by_country[:5].plot(ax=ax, kind='bar', color='pink')
    plt.savefig('tweets_country', format='png')

    #3 columns to the tweets DataFrame for keywords
    tweets['Beyonce'] = tweets['text'].apply(lambda tweet: word_in_text('Beyonce', tweet))
    tweets['Amine'] = tweets['text'].apply(lambda tweet: word_in_text('Amine', tweet))
    tweets['SJU'] = tweets['text'].apply(lambda tweet: word_in_text('SJU', tweet))

    #Prints amount of times mentioned
    print tweets['Beyonce'].value_counts()[True]
    print tweets['Amine'].value_counts()[True]
    print tweets['SJU'].value_counts()[True]


    print 'Analyzing most popular keyword:\n'
    key_word = ['Beyonce', 'Amine', 'SJU']
    tweet_by_mostmentioned = [tweets['Beyonce'].value_counts()[True], tweets['Amine'].value_counts()[True], tweets['SJU'].value_counts()[True]]
    x_pos = list(range(len(key_word)))
    width = 0.8
    fig, ax = plt.subplots()
    plt.bar(x_pos, tweet_by_mostmentioned, width, alpha=1, color='pink')
    ax.set_ylabel('Number of tweets', fontsize=15)
    ax.set_title('Most mentioned...', fontsize=10, fontweight='bold')
    ax.set_xticks([p + 0.4 * width for p in x_pos])
    ax.set_xticklabels(key_word)
    plt.grid()
    plt.savefig('tweet_by_mostmentioned', format='png')


if __name__=='__main__':
    main()
